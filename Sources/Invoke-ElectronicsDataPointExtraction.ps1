Start-Transcript -Path "$PSScriptRoot\..\Log\$(Get-Date -Format "yyyy-MM-dd-HHmmss").log"

Import-Module -Name "$PSScriptRoot\Module\ElectronicsDataHandling" -Force
Convert-ElectronicsDataFile -Verbose

Stop-Transcript