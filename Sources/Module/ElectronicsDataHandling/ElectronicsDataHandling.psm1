function Get-SelectorFromDialog {
    <#
    .SYNOPSIS

    .DESCRIPTION
    .NOTES
        https://learn.microsoft.com/en-us/powershell/scripting/samples/creating-a-custom-input-box?view=powershell-7.2
    #>
    [CmdletBinding()]
    param ()
    
    begin {}
    
    process {
        Add-Type -AssemblyName System.Windows.Forms
        Add-Type -AssemblyName System.Drawing

        $form = New-Object System.Windows.Forms.Form
        $form.Text = 'Data Entry Form'
        $form.Size = New-Object System.Drawing.Size(400, 200)
        $form.StartPosition = 'CenterScreen'

        $okButton = New-Object System.Windows.Forms.Button
        $okButton.Location = New-Object System.Drawing.Point(125, 120)
        $okButton.Size = New-Object System.Drawing.Size(75, 23)
        $okButton.Text = 'OK'
        $okButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
        $form.AcceptButton = $okButton
        $form.Controls.Add($okButton)

        $cancelButton = New-Object System.Windows.Forms.Button
        $cancelButton.Location = New-Object System.Drawing.Point(200, 120)
        $cancelButton.Size = New-Object System.Drawing.Size(75, 23)
        $cancelButton.Text = 'Cancel'
        $cancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
        $form.CancelButton = $cancelButton
        $form.Controls.Add($cancelButton)

        $label = New-Object System.Windows.Forms.Label
        $label.Location = New-Object System.Drawing.Point(10, 20)
        $label.Size = New-Object System.Drawing.Size(380, 40)
        $label.Text = "How many elements should be extracted?`n(Every n-th element)"
        $form.Controls.Add($label)

        $textBox = New-Object System.Windows.Forms.TextBox
        $textBox.Location = New-Object System.Drawing.Point(10, 60)
        $textBox.Size = New-Object System.Drawing.Size(30, 20)
        $form.Controls.Add($textBox)

        $form.Topmost = $true

        $form.Add_Shown({ $textBox.Select() })
        $result = $form.ShowDialog()

        if ($result -eq [System.Windows.Forms.DialogResult]::OK) {
            [int]$x = $textBox.Text
            $x
        }    
    }
    
    end {}
}

function Import-RequiredModules {
    [CmdletBinding()]
    param ()
    
    begin {}
    
    process {
        $RequiredModules = @('ImportExcel')
        foreach ($ModuleName in $RequiredModules) {
            
            if (-not (Get-Module $ModuleName)) {
                try {
                    Import-Module -Name $ModuleName -Verbose:$false -ErrorAction Stop
                    Write-Verbose "Imported module '$ModuleName'"
                } catch {
                    Write-Warning "Module '$ModuleName' could not be imported"
                    return
                }
            }
        }
    }
    
    end {}
}

function Convert-ElectronicsDataFile {
    <#
.SYNOPSIS

.DESCRIPTION

.NOTES
    
#>

    [CmdletBinding()]
    param (
        [Parameter()]
        [System.IO.FileInfo]
        $SourceFile,

        [Parameter()]
        [string]
        $OutputFilePath
    )

    begin {} 

    process {
        Import-RequiredModules

        #region Select input file
        if (-not ($Sourcefile)) {

            Add-Type -AssemblyName System.Windows.Forms
            $Property = @{
                InitialDirectory = [Environment]::GetFolderPath('Desktop')
                Title            = 'Select input file'
            }
            $FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property $Property
            $null = $FileBrowser.ShowDialog()
            $SourceFile = Get-Item -Path $FileBrowser.FileName
            
            Write-Verbose "Source file is '$($SourceFile.FullName)'"
        }
        $FileExtension = $SourceFile.Extension
        if ($FileExtension -notmatch '.txt|.xlsx|.csv') {
            Write-Warning "File format is not supported: '$FileExtension'"
            return
        }
        #endregion 
        Write-Output "What"

        #region Select output file
        if (-not ($OutputFilePath)) {
            $OutputFileName = "extracted-$($SourceFile.Name)"
        } else {
            $OutputFileName = $OutputFilePath
        }
            
        Add-Type -AssemblyName System.Windows.Forms
        $SaveProperty = @{
            InitialDirectory = $SourceFile.DirectoryName
            OverwritePrompt  = $True
            Filter           = "All files (*.*)|*.*"
            Title            = 'Save as'
            FileName         = $OutputFileName
        }
        $SaveFileDialog = New-Object System.Windows.Forms.SaveFileDialog -Property $SaveProperty
        $SaveStatus = $SaveFileDialog.ShowDialog()
            
        if ($SaveStatus -eq 'Cancel') {
            Write-Warning 'Save file dialog got cancelled'
            return
        }

        $OutputFilePath = $SaveFileDialog.FileName
        #endregion

        $Selector = Get-SelectorFromDialog


        Write-Output "Importing content from '$($SourceFile.FullName)'"

        switch ($FileExtension) {
            '.csv' { $Data = Import-CSV -Path $SourceFile.FullName }
            '.txt' { $Data = Get-Content -Path $SourceFile.FullName }
            '.xlsx' { $Data = Import-Excel -Path $SourceFile.FullName -NoHeader }# -AsText *}
        }
        
    
        $i = 0
        $Result = $Data | ForEach-Object {
            if ($i % $Selector -eq 0) {
                $_
            }
            $i++
        }
        switch ($FileExtension) {
            '.csv' {
                $Result | Export-CSV -Path $OutputFilePath -NoTypeInformation
            }
            '.txt' { 
                $null = New-Item -Path $OutputFilePath -Force
                $Result | Set-Content -Path $OutputFilePath
            }
            '.xlsx' { 
                $Result | Export-Excel -Path $OutputFilePath -NoHeader -ClearSheet
            }
        }
        Write-Verbose "Created file '$OutputFilePath'"
        Invoke-Item -Path (Get-Item -Path $OutputFilePath).DirectoryName
    }
    
    end {}
}

Export-ModuleMember -Function Convert-ElectronicsDataFile
