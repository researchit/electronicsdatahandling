powershell.exe -ExecutionPolicy Bypass -Command "& {[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12}"
powershell.exe -ExecutionPolicy Bypass -Command "& {Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force -Scope CurrentUser}"
powershell.exe -ExecutionPolicy Bypass -Command "& {Set-PSRepository -Name PSGallery -InstallationPolicy Trusted}	
powershell.exe -ExecutionPolicy Bypass -Command "& {Find-Module -Name 'ImportExcel' | Install-Module -Scope CurrentUser}"
powershell.exe -ExecutionPolicy Bypass -Command "& {$Files = Get-ChildItem -Path %cd%\* -Recurse -Include '*.ps*1'; Unblock-File -Path $Files.FullName}"