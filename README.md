# Electronics Data Handling

#### First time run ("Installation")

The <code>FirstTimeRun.bat</code> file prepares the local environment in the current user's scope so the module can access all its functionality.

#### Usage

The <code>ElectronicsDataPointExtraction.bat</code> file invokes a PowerShell function, which calls the logic of the <code>ElectronicsDataHandling</code> module.

The extraction function asks for an integer number <code>N</code>, which selects every <code>n-th</code> line of the input file.

#### Release History

##### Version 1.0.1

* Changed function: <code>Convert-ElectronicsDataFile</code><br>Improved Excel file handling<br>Introduced CSV file handling
* Added transcript logging

##### Version 1.0.0

* Added function: <code>Convert-ElectronicsDataFile</code>
